<?php

namespace Rooyesh\Wallet\interfaces;

use Illuminate\Support\Facades\Validator;
use Rooyesh\Wallet\models\TransactionMeta;
use Rooyesh\Wallet\models\wallet;
use Rooyesh\Wallet\models\transaction;
use Rooyesh\Wallet\Exceptions;
use \Illuminate\Validation\ValidationException;

class  TransactionInterface
{

    private $_inp;
    private $_type;
    private $wallet_interface;
    private $deposit;
    private $transaction;

    function __construct($wallet_interface = null, $_inp = null, $_type = null, $deposit = false)

    {
        $this->transaction = TransactionInterface::apply_transaction($wallet_interface, $_inp, $_type, $deposit);
    }

    static function validate_last_transaction($wallet_id)
    {
        $last_transaction = Transaction::where('wallet', '=', $wallet_id)->orderBy('created_at', 'DESC')->first();
        if ($last_transaction and $last_transaction->hash !== TransactionInterface::generate_hash($last_transaction)) {
            throw new Exceptions\LastTransactionValidateFailed();
        }
    }

    private static function generate_hash($transaction)
    {
        $last_transaction_hash = '';
        $last_user_transaction = Transaction::where('wallet', '=', $transaction->wallet)->orderBy('created_at', 'DESC');
        if ($last_user_transaction->count() and $last_user_transaction->first() != $transaction) {
            $last_transaction_hash = $last_user_transaction->first()->hash;
        }
        return hash('sha256',
            hash('sha256', $transaction->value) .
            hash('sha256', $transaction->balance) .
            hash('sha256', $transaction->wallet) .
            "yhoenszeaf" . $last_transaction_hash
        );
    }

    private static function set_hash($transaction)
    {
        $transaction->hash = TransactionInterface::generate_hash($transaction);
        $transaction->save();
    }

    public static function apply_transaction($wallet_interface, $_inp, $_type, $deposit, $meta = [])
    {
        $wallet = $wallet_interface->wallet;
        TransactionInterface::validate_last_transaction($wallet->user);
        #-------------Check Balance --------
        if (!$deposit) {
            if ($_type === 1 && $wallet->cash < $_inp['value']) {
                throw new Exceptions\NotEnoughCashBalance();

            } elseif ($_type === 2 && $wallet->credit < $_inp['value']) {
                throw new Exceptions\NotEnoughCreditBalance();

            }
        }
        $_inp['value'] = abs($_inp['value']) * ($deposit ? 1 : -1);
        $_inp['wallet'] = $wallet->id;
        $_inp['type'] = $_type;
        $_inp['balance'] = $wallet->cash + $wallet->credit + $_inp['value'];
        $transaction_validate_rules = [
            'wallet' => 'required',
            'type' => 'required',
            'expire_at' => 'date',
            'value' => 'integer | required',
            'note' => 'string',
            'balance' => 'integer',
        ];
        $transaction_validator = Validator::make($_inp, $transaction_validate_rules);
        if ($transaction_validator->failed()) {
            throw ValidationException::withMessages($transaction_validator->errors()->all());
        }
        $transaction = Transaction::create($_inp);
        $transaction->save();
        TransactionInterface::set_hash($transaction);
        $meta['transaction'] = $transaction->id;
        $transaction_metas = TransactionMeta::create($meta);
        if ($_inp['type'] == 1) {
            $wallet->cash += $_inp['value'];
        } elseif ($_inp['type'] == 2) {
            $wallet->credit += $_inp['value'];
        }
        $wallet->save();
        $wallet_interface->set_hash();
        return $transaction;
    }

}
