<?php

namespace Rooyesh\Wallet\interfaces;

use Rooyesh\Wallet\models\wallet;
use Rooyesh\Wallet\models\transaction;
use Rooyesh\Wallet\Exceptions;


class WalletInterface
{
    public $wallet = null;
    public $owner_id = null;
    public $owner_type = null;

    function __construct($owner_id, $owner_type)
    {
        $this->owner_id = $owner_id;
        $this->owner_type = $owner_type;

        if (wallet::where([
            ['owner', '=', $owner_id],
            ['owner_type', '=', $owner_type],
        ])->count()) {
            $this->wallet = wallet::where([
                ['owner', '=', $owner_id],
                ['owner_type', '=', $owner_type],
            ])->first();
            $this->validate();
        } else {
            $this->wallet = wallet::create([
                'owner' => $owner_id,
                'owner_type' => $owner_type,
                'credit' => 0,
                'cash' => 0
            ]);
            $this->set_hash();
        }
    }

    public function validate(): bool
    {
        if ($this->wallet->hash !== $this->generate_hash()) {
            throw new Exceptions\WalletValidateFailed($this->wallet);
        }
        return true;
    }

    private function generate_hash(): string
    {
        $last_transaction = Transaction::where('wallet', '=', $this->wallet->id)->orderBy('created_at', 'DESC')->first();
        $last_transaction_hash = $last_transaction ? hash('sha256', $last_transaction->hash) : '';
        return hash('sha256', hash('sha256', $this->wallet->credit) .
            hash('sha256', $this->wallet->cash) . hash('sha256', $this->wallet->owner) . $last_transaction_hash . "yhoenszeaf");
    }

    public function set_hash(): bool
    {
        $this->wallet->hash = $this->generate_hash();
        $this->wallet->save();
        return true;
    }
}

