<?php

namespace Rooyesh\Wallet\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $guarded=['hash'];

    use HasFactory;
}
