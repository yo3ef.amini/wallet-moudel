<?php

namespace Rooyesh\Wallet\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionMeta extends Model
{
    use HasFactory;
    protected $guarded=[];

}
