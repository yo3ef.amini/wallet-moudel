<?php

namespace Rooyesh\Wallet\Exceptions;

use Exception;

class WalletValidateFailed extends Exception
{
    private $wallet_id = null;

    public function __construct($wallet, $user = null)
    {
        parent::__construct();
        $this->wallet_id = $wallet->id;
    }

    public function context()
    {
        return [' ' => $this->wallet_id];
    }
}
