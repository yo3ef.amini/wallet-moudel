<?php

namespace Rooyesh\Wallet\Exceptions;

use Exception;

class NotEnoughCreditBalance extends Exception
{
}
