<?php

namespace Rooyesh\Wallet\Exceptions;

use Exception;

class NotEnoughCashBalance extends Exception
{
}
