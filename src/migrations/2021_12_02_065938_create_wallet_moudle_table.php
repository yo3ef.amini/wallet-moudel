<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletMoudleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->id();
            $ownerTypes = [
                1 => "user",
                2 => "shop",
            ];
            $table->enum('owner_type', array_keys($ownerTypes))->default(1);
            $table->timestamps();
            $table->bigInteger('owner');
            $table->bigInteger('cash');
            $table->string('hash', 128)->nullable(true);
            $table->bigInteger('credit');
            $table->unique(['owner', 'owner_type']);

        });

        Schema::create('transactions', function (Blueprint $table) {
            $transactionTypes = [
                1 => "cash",
                2 => "credit",
                3 => "expire",
            ];
            $table->id();
            $table->bigInteger('wallet', false, true);
            $table->enum('transaction_type', array_keys($transactionTypes))->default(1);
            $table->timestamps();
            $table->bigInteger('value');
            $table->dateTime('expire_at')->nullable(true);
            $table->string('hash', 128)->nullable(true);
            $table->text('note')->nullable(true);
        });

        Schema::create('transaction_metas', function (Blueprint $table) {
            $table->bigInteger('transaction', false, true);
            $table->id();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
