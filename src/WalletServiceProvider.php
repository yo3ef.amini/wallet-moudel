<?php

namespace Rooyesh\Wallet;

use Illuminate\Support\ServiceProvider;

class WalletServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . "/migrations");
    }

    public function register()
    {

    }
}
